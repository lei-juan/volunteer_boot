package com.volunteer.pc.vo;

import com.volunteer.admin.vo.UserVo;
import com.volunteer.common.entity.Article;
import com.volunteer.common.entity.ArticleUser;
import com.volunteer.common.entity.User;
import lombok.Data;

import java.util.List;

@Data
public class ArticleUserVo{
    private Article article;
    private List<UserView> user;
}
