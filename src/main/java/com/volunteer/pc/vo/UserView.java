package com.volunteer.pc.vo;

import com.volunteer.common.entity.User;
import lombok.Data;

@Data
public class UserView extends User {
    private Integer status;
}
