package com.volunteer.pc.vo;
import com.volunteer.common.entity.Comment;
import com.volunteer.common.entity.User;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * comment实体类
 * @author Administrator
 *
 */
@Data
public class CommentVo extends Comment implements Serializable{
	private User user;
}
