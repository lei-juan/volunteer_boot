package com.volunteer.pc.vo;

import com.volunteer.common.entity.*;

public class ArticleVo extends Article {
    private Lable lablel;
    private ArticleType articleType;
    private User user;
    private ArticleUser articleUser;
    public Lable getLablel() {
        return lablel;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setLablel(Lable lablel) {
        this.lablel = lablel;
    }

    public ArticleType getArticleType() {
        return articleType;
    }

    public void setArticleType(ArticleType articleType) {
        this.articleType = articleType;
    }

    public ArticleUser getArticleUser() {
        return articleUser;
    }

    public void setArticleUser(ArticleUser articleUser) {
        this.articleUser = articleUser;
    }
}
