package com.volunteer.pc.service;


import com.volunteer.common.entity.ArticleUser;
import com.volunteer.pc.vo.ArticleUserVo;

/**
 * Dictionary业务逻辑层
 */
public interface PcArticleUserService {
    /**
     * 参加活动
     * @param articleId
     */
    void campaign(Integer articleId);
    /**
     *
     * 活动人数列表
     */
    ArticleUserVo activityList(Integer articleId,Integer status);

    /**
     * 更新状态
     * @param articleUser
     *
     */
    void updateStatusParticipants(ArticleUser articleUser);


}
