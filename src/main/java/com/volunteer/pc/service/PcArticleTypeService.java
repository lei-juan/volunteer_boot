package com.volunteer.pc.service;

import com.volunteer.common.entity.ArticleType;
import com.volunteer.common.utils.PageResult;

/**
 * lable业务逻辑层
 */
public interface PcArticleTypeService {
    public PageResult<ArticleType> findPage(int page, int size);
}
