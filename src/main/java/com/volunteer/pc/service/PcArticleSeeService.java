package com.volunteer.pc.service;

import com.volunteer.admin.vo.ArticleSeeVO;
import com.volunteer.common.entity.ArticleSee;

import java.util.List;

/**
 * @author : TBH
 * @date : 2020-11-05-13:57
 */
public interface PcArticleSeeService {
    void save(ArticleSee articleSee);

    List<ArticleSeeVO> findRecent(int page,int size);
}
