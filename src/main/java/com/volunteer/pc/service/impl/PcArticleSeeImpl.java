package com.volunteer.pc.service.impl;

import com.volunteer.admin.vo.ArticleSeeVO;
import com.volunteer.common.entity.ArticleSee;
import com.volunteer.common.mapper.ArticleSeeMapper;
import com.volunteer.pc.service.PcArticleSeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author : TBH
 * @date : 2020-11-05-13:57
 */
@Service
public class PcArticleSeeImpl implements PcArticleSeeService {

    @Autowired
    private ArticleSeeMapper articleSeeMapper;

    @Override
    public void save(ArticleSee articleSee) {
        //如果是这个表里面已经存在了这个用户的id就更新一下时间就好了
        Long id = articleSeeMapper.findIdByUid(articleSee.getUid());
        //如果这个id有值说明存在,就更新一下时间,如果不存在则添加这条数据
        if(Objects.isNull(id)){
            articleSeeMapper.insert(articleSee);
        }
        if(!Objects.isNull(id)){
            articleSee.setId(id);
            articleSeeMapper.updateByPrimaryKeySelective(articleSee);
        }
    }

    @Override
    public List<ArticleSeeVO> findRecent(int page,int size) {
        return articleSeeMapper.findRecent(page,size);
    }
}
