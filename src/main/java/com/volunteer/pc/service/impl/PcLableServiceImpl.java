package com.volunteer.pc.service.impl;
import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageInfo;
import com.volunteer.common.entity.Lable;
import com.volunteer.common.mapper.LableMapper;
import com.volunteer.common.utils.PageResult;
import com.volunteer.pc.service.PcLableService;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.Map;

@Service
public class PcLableServiceImpl implements PcLableService {

    @Autowired
    private LableMapper lableMapper;


    @Override
    public PageResult<Lable> queryLableByPage(Integer page, Integer size, Map<String, Object> rules) {
        PageHelper.startPage(page,size,rules.getOrDefault("sort","sort_code desc,create_time desc").toString());

        PageInfo<Lable> pageInfo=new PageInfo<>(lableMapper.selectByExample(this.createExample(rules)));
        return new PageResult<>(pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Lable.class);
        Example.Criteria criteria = example.createCriteria();
        if(CollUtil.isNotEmpty(searchMap)){
            //
        }
        return example;
    }
}
