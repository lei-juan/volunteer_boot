package com.volunteer.pc.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.volunteer.common.entity.ArticleType;
import com.volunteer.common.mapper.ArticleTypeMapper;
import com.volunteer.common.utils.PageResult;
import com.volunteer.pc.service.PcArticleTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PcArticleTypeServiceImpl implements PcArticleTypeService {

    @Autowired
    private ArticleTypeMapper articleTypeMapper;

    @Override
    public PageResult<ArticleType> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        PageInfo<ArticleType> info = new PageInfo<>(articleTypeMapper.selectAll());
        return new PageResult<>(info.getTotal(),info.getList());
    }
}
