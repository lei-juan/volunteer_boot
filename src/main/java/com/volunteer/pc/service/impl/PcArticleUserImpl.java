package com.volunteer.pc.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.volunteer.admin.security.UserDetailsVo;
import com.volunteer.common.entity.Article;
import com.volunteer.common.entity.ArticleUser;
import com.volunteer.common.entity.User;
import com.volunteer.common.mapper.ArticleMapper;
import com.volunteer.common.mapper.ArticleUserMapper;
import com.volunteer.common.mapper.UserMapper;
import com.volunteer.pc.service.PcArticleUserService;
import com.volunteer.pc.vo.ArticleUserVo;
import com.volunteer.pc.vo.UserView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author : TBH
 * @date : 2020-11-05-13:57
 */
@Service
public class PcArticleUserImpl implements PcArticleUserService {
    @Autowired
    private ArticleUserMapper articleUserMapper;
    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private UserMapper userMapper;
    @Override
    public void campaign(Integer articleId) {
        ArticleUser articleUser=new ArticleUser();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Long userId = ((UserDetailsVo) authentication.getPrincipal()).getId();
        articleUser.setArticleId(articleId);
        articleUser.setStatus(0);
        articleUser.setUserId(userId);


        Example example=new Example(ArticleUser.class);
        example.createCriteria().andEqualTo("articleId",articleId).andEqualTo("userId",userId);

        List<ArticleUser> articleUsers = articleUserMapper.selectByExample(example);

        if(CollUtil.isNotEmpty(articleUsers)){
            throw new RuntimeException("你已经参加过活动了不能重复参加");
        }
        articleUserMapper.insert(articleUser);
    }

    @Override
    public ArticleUserVo activityList(Integer articleId, Integer status) {
        ArticleUserVo vo=new ArticleUserVo();
        Article article = articleMapper.selectByPrimaryKey(articleId);
        if(Objects.isNull(article)){
            throw new RuntimeException("活动已经不存在刷新页面试试！");
        }
        vo.setArticle(article);
        Example example=new Example(ArticleUser.class);
        if(status==null){
            example.createCriteria().andEqualTo("articleId",articleId);
        }else{
            example.createCriteria().andEqualTo("articleId",articleId)
            .andEqualTo("status",status);
        }
        List<ArticleUser> articleUsers = articleUserMapper.selectByExample(example);
        List<UserView> arr=new ArrayList<>();
        articleUsers.forEach(e->{
            UserView userView=new UserView();
            User user = userMapper.selectByPrimaryKey(e.getUserId());
            BeanUtils.copyProperties(user,userView);
            userView.setStatus(e.getStatus());
            arr.add(userView);
        });
        vo.setUser(arr);
        return vo;
    }

    @Override
    public void updateStatusParticipants(ArticleUser articleUser) {
        Example example=new Example(ArticleUser.class);
        example.createCriteria().andEqualTo("articleId",articleUser.getArticleId())
                .andEqualTo("userId",articleUser.getUserId());
        ArticleUser updateUser=new ArticleUser();
        updateUser.setStatus(articleUser.getStatus());
        articleUserMapper.updateByExampleSelective(updateUser,example);
    }
}
