package com.volunteer.pc.service.impl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.volunteer.admin.service.UserService;
import com.volunteer.common.entity.Comment;
import com.volunteer.common.entity.User;
import com.volunteer.common.mapper.CommentMapper;
import com.volunteer.common.mapper.UserMapper;
import com.volunteer.common.utils.PageResult;
import com.volunteer.pc.service.PcCommentService;
import com.volunteer.pc.vo.CommentVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PcCommentServiceImpl implements PcCommentService {

    @Autowired
    private CommentMapper commentMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Comment> findAll() {
        return commentMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Comment> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Comment> comments = (Page<Comment>) commentMapper.selectAll();
        return new PageResult<Comment>(comments.getTotal(),comments.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Comment> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return commentMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Comment> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Comment> comments = (Page<Comment>) commentMapper.selectByExample(example);
        return new PageResult<Comment>(comments.getTotal(),comments.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Comment findById(Integer id) {
        return commentMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param comment
     */
    public void add(Comment comment) {
        commentMapper.insert(comment);
    }

    /**
     * 修改
     * @param comment
     */
    public void update(Comment comment) {
        commentMapper.updateByPrimaryKeySelective(comment);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(Integer id) {
        commentMapper.deleteByPrimaryKey(id);
    }

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<CommentVo> findArticles(Integer parentId, String type) {
        Example example=new Example(Comment.class);
        example.createCriteria().andEqualTo("parentId",parentId)
        .andEqualTo("type",type);

       return builldList(example);
    }

    @Override
    public List<CommentVo> querySumComent(Integer rootId, String type) {
        Example example=new Example(Comment.class);
        example.createCriteria().andEqualTo("rootId",rootId)
                .andEqualTo("type",type);

     return builldList(example);
    }
    public List<CommentVo> builldList( Example example){
        return  commentMapper.selectByExample(example).stream().map(e->{
            CommentVo commentVo=new CommentVo();

            BeanUtils.copyProperties(e,commentVo);
            Example example1=new Example(User.class);
            example1.createCriteria().andEqualTo("username",e.getUsername());
            User user = userMapper.selectOneByExample(example1);
            commentVo.setUser(user);
            return commentVo;
        }).collect(Collectors.toList());
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Comment.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 账号
            if(searchMap.get("username")!=null && !"".equals(searchMap.get("username"))){
                criteria.andLike("username","%"+searchMap.get("username")+"%");
            }
            // IP地址
            if(searchMap.get("ip")!=null && !"".equals(searchMap.get("ip"))){
                criteria.andLike("ip","%"+searchMap.get("ip")+"%");
            }
            // 位置
            if(searchMap.get("address")!=null && !"".equals(searchMap.get("address"))){
                criteria.andLike("address","%"+searchMap.get("address")+"%");
            }
            // 1帖子0代表评论
            if(searchMap.get("type")!=null && !"".equals(searchMap.get("type"))){
                criteria.andLike("type","%"+searchMap.get("type")+"%");
            }

            // id
            if(searchMap.get("id")!=null ){
                criteria.andEqualTo("id",searchMap.get("id"));
            }
            // 帖子id或者评论id
            if(searchMap.get("parentId")!=null ){
                criteria.andEqualTo("parentId",searchMap.get("parentId"));
            }

        }
        return example;
    }

}
