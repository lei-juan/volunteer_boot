package com.volunteer.pc.service.impl;
import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageInfo;
import com.volunteer.common.entity.Article;
import com.volunteer.common.entity.ArticleType;
import com.volunteer.common.entity.Lable;
import com.volunteer.common.entity.User;
import com.volunteer.common.mapper.ArticleMapper;
import com.volunteer.common.mapper.ArticleTypeMapper;
import com.volunteer.common.mapper.LableMapper;
import com.volunteer.common.mapper.UserMapper;
import com.volunteer.common.utils.PageResult;
import com.volunteer.pc.service.PcArticleService;
import com.volunteer.pc.vo.ArticleVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PcArticleServiceImpl implements PcArticleService {

    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private LableMapper lableMapper;

    @Autowired
    private ArticleTypeMapper articleTypeMapper;

    @Override
    public PageResult<ArticleVo> queryArticleByPage(Integer page, Integer size, Map<String, Object> rules) {
        PageHelper.startPage(page,size,rules.getOrDefault("sort","is_top desc,sort_code desc,create_time desc").toString());

        PageInfo<Article> pageInfo=new PageInfo<>(articleMapper.selectByExample(this.createExample(rules)));
        List<ArticleVo> list=new ArrayList<>();
        if(CollUtil.isNotEmpty(pageInfo.getList())){
            List<String> lableCodes = pageInfo.getList().stream()
                    .map(Article::getLableCode)
                    .distinct()
                    .collect(Collectors.toList());

            List<String> typeList = pageInfo.getList().stream()
                    .map(Article::getTypeCode)
                    .distinct()
                    .collect(Collectors.toList());

            Example example=new Example(Lable.class);
            example.createCriteria().andIn("lableCode",lableCodes);
            Map<String, List<Lable>> map = lableMapper.selectByExample(example).stream()
                    .collect(Collectors.groupingBy(Lable::getLableCode));
            Example example2=new Example(ArticleType.class);
            example2.createCriteria().andIn("typeCode",typeList);
            Map<String, List<ArticleType>> map2 = articleTypeMapper.selectByExample(example2).stream()
                    .collect(Collectors.groupingBy(ArticleType::getTypeCode));
            list= pageInfo.getList().stream().map(e -> {
                ArticleVo vo = new ArticleVo();
                BeanUtils.copyProperties(e,vo);
                if (map.containsKey(e.getLableCode())) {
                    vo.setLablel(map.get(e.getLableCode()).get(0));
                }
                if (map2.containsKey(e.getTypeCode())) {
                    vo.setArticleType(map2.get(e.getTypeCode()).get(0));
                }
                return vo;
            }).collect(Collectors.toList());
        }

        return new PageResult<>(pageInfo.getTotal(),list);
    }

    @Override
    public int saveArticle(Article article) {
        article.setIsTop(0);
        article.setLikes(0);
        article.setComments(0);
        article.setSortCode(0);
        article.setCreateTime(new Date());
        article.setLastCommentsTime(new Date());
        article.setViews(0);
        return articleMapper.insert(article);
    }

    @Override
    public ArticleVo getArticle(Integer id) {
        Article article = articleMapper.selectByPrimaryKey(id);
        ArticleVo vo=new ArticleVo();
        BeanUtils.copyProperties(article,vo);
        Example example=new Example(User.class);
        example.createCriteria().andEqualTo("username",article.getUsername());
        User user = userMapper.selectOneByExample(example);
        vo.setUser(user);
        return vo;
    }

    @Override
    public int updateArticle(Article article) {
        return articleMapper.updateByPrimaryKeySelective(article);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Article.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 类型
            if(searchMap.get("type")!=null && !"".equals(searchMap.get("type"))){
                criteria.andEqualTo("type",searchMap.get("type"));
            }
            // top
            if(searchMap.get("isTop")!=null && !"".equals(searchMap.get("isTop"))){
                criteria.andEqualTo("isTop",searchMap.get("isTop"));
            }
            // lable
            if(searchMap.get("lableCode")!=null && !"".equals(searchMap.get("lableCode"))){
                criteria.andEqualTo("lableCode",searchMap.get("lableCode"));
            }
            //status
            if(searchMap.get("status")!=null && !"".equals(searchMap.get("status"))){
                criteria.andEqualTo("status",searchMap.get("status"));
            }
            //articleIds
            List<Long> articleIds = (List<Long>) searchMap.get("articleIds");
            if(CollUtil.isNotEmpty(articleIds)){
                criteria.andIn("id",articleIds);
            }

        }
        return example;
    }
}
