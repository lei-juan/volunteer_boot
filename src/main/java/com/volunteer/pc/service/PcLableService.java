package com.volunteer.pc.service;
import com.volunteer.common.entity.Lable;
import com.volunteer.common.utils.PageResult;

import java.util.*;

/**
 * lable业务逻辑层
 */
public interface PcLableService {
    PageResult<Lable> queryLableByPage(Integer page, Integer size, Map<String,Object> rules);

}
