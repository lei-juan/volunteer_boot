package com.volunteer.pc.service;

import com.volunteer.common.entity.Comment;
import com.volunteer.common.utils.PageResult;
import com.volunteer.pc.vo.CommentVo;

import java.util.*;

/**
 * comment业务逻辑层
 */
public interface PcCommentService {


    public List<Comment> findAll();


    public PageResult<Comment> findPage(int page, int size);


    public List<Comment> findList(Map<String,Object> searchMap);


    public PageResult<Comment> findPage(Map<String,Object> searchMap,int page, int size);


    public Comment findById(Integer id);

    public void add(Comment comment);


    public void update(Comment comment);


    public void delete(Integer id);


    List<CommentVo> findArticles(Integer parentId, String type);

    List<CommentVo> querySumComent(Integer parentId, String type);
}
