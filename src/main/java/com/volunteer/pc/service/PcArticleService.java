package com.volunteer.pc.service;
import com.volunteer.common.entity.Article;
import com.volunteer.common.utils.PageResult;
import com.volunteer.pc.vo.ArticleVo;

import java.util.*;

/**
 * article业务逻辑层
 */
public interface PcArticleService {

    PageResult<ArticleVo> queryArticleByPage(Integer page, Integer size, Map<String,Object> rules);

    int saveArticle(Article article);

    ArticleVo getArticle(Integer id);

    int updateArticle(Article article);

}
