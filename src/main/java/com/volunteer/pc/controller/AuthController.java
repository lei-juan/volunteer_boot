package com.volunteer.pc.controller;

import cn.hutool.core.collection.CollUtil;
import com.volunteer.admin.security.UserDetailsVo;
import com.volunteer.admin.service.UserService;
import com.volunteer.common.entity.Article;
import com.volunteer.common.entity.ArticleUser;
import com.volunteer.common.entity.User;
import com.volunteer.common.mapper.ArticleUserMapper;
import com.volunteer.common.utils.PageResult;
import com.volunteer.common.utils.ResponseResult;
import com.volunteer.pc.service.PcArticleService;
import com.volunteer.pc.vo.ArticleVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private PcArticleService articleService;

    @Autowired
    private ArticleUserMapper articleUserMapper;
    @RequestMapping("/isLogin")
    public ResponseResult<String> isLogin() {
        return ResponseResult.success();
    }

    /**
     * 获取用户信息
     * @return
     */
    @RequestMapping("/getUserInfo")
    public ResponseResult<User> getUserInfo() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = authentication.getName();
        User user = userService.findUserInfoByUsername(name);
        user.setPassword(null);
        return ResponseResult.success(user);
    }

    /**
     * 保存修改用户的信息
     * @return
     */
    @RequestMapping("/saveUser")
    public ResponseResult<String> saveUser(@RequestBody User user) {
        userService.update(user);
        return ResponseResult.success();
    }


    /**
     * 发表文章
     * @param article
     * @return
     * @throws IOException
     */
    @PostMapping("/saveArticle")
    public ResponseResult saveArticle(@RequestBody Article article) throws IOException {
        if(article.getId()!=null){
            articleService.updateArticle(article);
        }else{
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
            if(authorities.contains(new SimpleGrantedAuthority("admin"))){
                article.setStatus(1);
            }else{
                article.setStatus(0);
            }
            article.setUsername(authentication.getName());
            articleService.saveArticle(article);
        }

        return ResponseResult.success();
    }

    /**
     * 获取我的文章
     * @param page page
     * @param size size
     * @throws IOException
     */
    @GetMapping("/queryMyArticles/{page}/{size}")
    public PageResult<ArticleVo> queryMyArticles(@PathVariable("page")Integer page, @PathVariable("size")Integer size, @RequestParam(value = "lable",required = false)String lable) throws IOException {
        Map<String, Object> searchMap = new HashMap<>();
        searchMap.put("status","1");
        searchMap.put("lableCode",lable);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Long id = ((UserDetailsVo) authentication.getPrincipal()).getId();

        Example example=new Example(ArticleUser.class);

        example.createCriteria().andEqualTo("userId",id);

        List<ArticleUser> userList = articleUserMapper.selectByExample(example);


        Map<Integer, List<ArticleUser>> map = userList.stream().collect(Collectors.groupingBy(ArticleUser::getArticleId));
        searchMap.put("articleIds",userList.stream().map(ArticleUser::getArticleId).collect(Collectors.toList()));
        PageResult<ArticleVo> pageResult = articleService.queryArticleByPage(page, size, searchMap);
        if(CollUtil.isNotEmpty(map)){

            pageResult.getRows().stream().peek(e->e.setArticleUser(map.get(e.getId()).get(0))).collect(Collectors.toList());
        }
        return pageResult;
    }
}

