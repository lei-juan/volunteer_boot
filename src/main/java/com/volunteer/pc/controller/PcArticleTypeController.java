package com.volunteer.pc.controller;

import com.volunteer.admin.service.ArticleTypeService;
import com.volunteer.common.entity.ArticleType;
import com.volunteer.common.utils.PageResult;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/pcArticleType")
@Api(tags = "pc前端类型调用接口")
public class PcArticleTypeController {

    @Autowired
    private ArticleTypeService articleTypeService;

    /**
     * 获取文章类型
     * @param page page
     * @param size size
     * @throws IOException
     */
    @GetMapping("/query/{page}/{size}")
    public PageResult<ArticleType> queryArticles(@PathVariable("page")Integer page, @PathVariable("size")Integer size) throws IOException {
        return articleTypeService.findPage(page, size);
    }

}
