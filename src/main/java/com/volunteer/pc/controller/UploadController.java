package com.volunteer.pc.controller;

import com.volunteer.common.config.MyConfig;
import com.volunteer.common.constant.UploadConstant;
import com.volunteer.common.utils.FastDFSClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private FastDFSClientUtil fastDFSClientUtil;

    @Autowired
    private MyConfig myConfig;

    @Value("${enable.local.upload.file}")
    private boolean enable;

    @PostMapping("/native")
    public String nativeUpload(@RequestParam("file") MultipartFile file) {
        long currentTimeMillis=System.currentTimeMillis();
        String path =myConfig.getProfile();
        String filePath =path +"\\"+currentTimeMillis%20+"\\"+currentTimeMillis+"\\"+ file.getOriginalFilename();
        File desFile = new File(filePath);
        if(!desFile.exists()){
            desFile.mkdirs();
        }
        try {
            file.transferTo(desFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("path:---"+filePath);
        return "http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"\\profile"+"\\"+currentTimeMillis%20+"\\"+currentTimeMillis+"\\"+file.getOriginalFilename();
    }

    @PostMapping("/serverUpload")
    public String serverUpload(@RequestParam("file") MultipartFile file) throws IOException {
        if(enable){
            return nativeUpload(file);
        }
        return fastDFSClientUtil.uploadFile(file);
    }

}