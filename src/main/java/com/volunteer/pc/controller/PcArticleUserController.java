package com.volunteer.pc.controller;

import com.volunteer.common.entity.ArticleUser;
import com.volunteer.common.utils.ResponseResult;
import com.volunteer.pc.service.PcArticleUserService;
import com.volunteer.pc.vo.ArticleUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/pcArticleUser")
public class PcArticleUserController {


    @Autowired
    private PcArticleUserService pcArticleUserService;

    /**
     * 参加活动
     * @throws IOException
     */
    @GetMapping("/join")
    public ResponseResult<String> queryPopularArticles(@RequestParam("articleId")Integer articleId) throws IOException {

        pcArticleUserService.campaign(articleId);
        return ResponseResult.success();
    }

    /**
     * 参加活动
     * @throws IOException
     */
    @GetMapping("/activityList")
    public ResponseResult<ArticleUserVo> activityList(@RequestParam("articleId")Integer articleId) throws IOException {
        ArticleUserVo articleUserVo = pcArticleUserService.activityList(articleId, null);
        return ResponseResult.success(articleUserVo);
    }


    /**
     * 已审核通过的活动人
     * @throws IOException
     */
    @GetMapping("/activeActivityList")
    public ResponseResult<ArticleUserVo> activeActivityList(@RequestParam("articleId")Integer articleId) throws IOException {
        ArticleUserVo articleUserVo = pcArticleUserService.activityList(articleId, 1);
        return ResponseResult.success(articleUserVo);
    }


    /**
     * 更新状态
     * @throws IOException
     */
    @PostMapping("/updateStatusParticipants")
    public ResponseResult<String> updateStatusParticipants(@RequestBody ArticleUser articleUser) throws IOException {
        pcArticleUserService.updateStatusParticipants(articleUser);
        return ResponseResult.success();
    }




}
