package com.volunteer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import tk.mybatis.spring.annotation.MapperScan;

@Import(cn.hutool.extra.spring.SpringUtil.class)
@SpringBootApplication
@MapperScan("com.volunteer.common.mapper")
public class InterviewResourcesApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterviewResourcesApplication.class, args);
    }




}