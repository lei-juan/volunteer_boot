package com.volunteer.admin.security;

import cn.hutool.json.JSONUtil;
import com.volunteer.common.utils.JwtResult;
import com.volunteer.common.utils.SendMailUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ExecutorService;

/**
 * @author volunteer
 * @date 2019/10/16 20:28 jwt成功生成报token
 */
@Slf4j
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private SendMailUtils.JwtTokenUtils jwtTokenUtils;


    @Value("${login.url}")
    private String loginUrl;

    @Value("${logout.url}")
    private String logoutUrl;

    @Autowired
    private ExecutorService logExecutorService;
    /**
     * 登录验证通过生产jwt返回客户端
     * @param request request
     * @param response response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        String jwt = JSONUtil.toJsonStr(JwtResult.success(jwtTokenUtils.generateAccessToken(authentication)));
        log.info("***登录成功***jwt:{}",jwt);
        response.getWriter().print(jwt);
        response.getWriter().flush();
        response.getWriter().close();
    }
}
