package com.volunteer.admin.security;

import cn.hutool.json.JSONUtil;
import com.volunteer.common.utils.JwtResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author volunteer
 * @date 2020/5/14 9:41
 */
@Slf4j
public class LoginFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        String errorMsg=exception.getMessage();
        if(exception instanceof BadCredentialsException){
            errorMsg="账号密码错误";
        }

        String jwt = JSONUtil.toJsonStr(JwtResult.error(errorMsg));
        log.info("***账号密码错误***jwt:{}",jwt);
        response.getWriter().print(jwt);
        response.getWriter().flush();
        response.getWriter().close();
    }
}
