package com.volunteer.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author : TBH
 * @date : 2020-11-05-14:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
public class ArticleSeeVO implements Serializable {

    /**
     * id
     */
    private Long id;

    /**
     * 用户的头像
     */
    private String avatar;

    /**
     * 用户名
     */
    private String username;
    /**
     * 用户名
     */
    private String name;

    /**
     * 访问时间
     */
    private Date seeTime;
}
