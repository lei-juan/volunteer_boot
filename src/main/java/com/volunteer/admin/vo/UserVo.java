package com.volunteer.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;

/**
 * @author volunteer
 * @date 2020/5/13 14:13
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserVo {
    /**
     * 用户名id
     */
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 注册手机号
     */
    private String phone;

    /**
     * 注册邮箱
     */
    private String email;

    private Date created;

    private Date updated;
    /**
     * 头像
     */
    @Column(name = "avatar")
    private String avatar;
    /**
     * 简介
     */
    @Column(name = "introduction")
    private String introduction;
    /**
     * 名字
     */
    @Column(name = "name")
    private String name;
    /**
     * 角色列表
     */
    private List<RoleVo> roles;
}
