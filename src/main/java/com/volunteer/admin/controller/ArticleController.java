package com.volunteer.admin.controller;

import com.volunteer.admin.service.ArticleService;
import com.volunteer.common.entity.Article;
import com.volunteer.common.utils.PageResult;
import com.volunteer.common.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping("/article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    /**
     * 返回全部article表记录
     * @return
     */
    @GetMapping("/findAll")
    public List<Article> findAll(){
        return articleService.findAll();
    }


    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    @GetMapping("/findPage")
    public PageResult<Article> findPage(int page, int size){
        return articleService.findPage(page, size);
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    @PostMapping("/findList")
    public List<Article> findList(@RequestBody Map<String,Object> searchMap){
        return articleService.findList(searchMap);
    }
    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */

    @PostMapping("/findPage")
    public PageResult<Article> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  articleService.findPage(searchMap,page,size);
    }
      /**
     * 根据Id查询
     * @param id
     * @return
     */
    @GetMapping("/findById")
    public Article findById(Integer id){
        return articleService.findById(id);
    }


    /**
     * 新增
     * @param article
     */
    @PostMapping("/add")
    public ResponseResult add(@RequestBody Article article){
        articleService.add(article);
        return new ResponseResult();
    }
    /**
     * 修改
     * @param article
     */
    @PostMapping("/update")
    public ResponseResult update(@RequestBody Article article){
        articleService.update(article);
        return new ResponseResult();
    }
    /**
     *  删除
     * @param id
     */
    @GetMapping("/delete")
    public ResponseResult delete(Integer id){
        articleService.delete(id);
        return new ResponseResult();
    }

}
