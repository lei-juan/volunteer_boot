package com.volunteer.admin.controller;

import com.volunteer.common.utils.ResponseResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一异常处理类
 */
@ControllerAdvice
public class BaseExceptionHandler {

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public ResponseResult<String> error(Throwable e) {
        e.printStackTrace();
        System.out.println("调用了公共异常处理类");
        return ResponseResult.error(e.getMessage());
    }
}
