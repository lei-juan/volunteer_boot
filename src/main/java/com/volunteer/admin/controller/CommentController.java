package com.volunteer.admin.controller;

import cn.hutool.core.collection.CollUtil;
import com.volunteer.common.entity.Comment;
import com.volunteer.common.utils.PageResult;
import com.volunteer.common.utils.ResponseResult;
import com.volunteer.pc.service.PcCommentService;
import com.volunteer.pc.vo.CommentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PcCommentService pcCommentService;

    /**
     * 返回全部comment表记录
     * @return
     */
    @GetMapping("/findAll")
    public List<Comment> findAll(){
        return pcCommentService.findAll();
    }


    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    @GetMapping("/findPage")
    public PageResult<Comment> findPage(int page, int size){
        return pcCommentService.findPage(page, size);
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    @PostMapping("/findList")
    public List<Comment> findList(@RequestBody Map<String,Object> searchMap){
        return pcCommentService.findList(searchMap);
    }
    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */

    @PostMapping("/findPage")
    public PageResult<Comment> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  pcCommentService.findPage(searchMap,page,size);
    }
      /**
     * 根据Id查询
     * @param id
     * @return
     */
    @GetMapping("/findById")
    public Comment findById(Integer id){
        return pcCommentService.findById(id);
    }


    /**
     * 新增
     * @param comment
     */
    @PostMapping("/add")
    public ResponseResult add(@RequestBody Comment comment, HttpServletRequest request){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        comment.setCommentTime(new Date());
        comment.setUsername(authentication.getName());
        comment.setIp(request.getRemoteAddr());
        comment.setAddress("本机");
        ResponseEntity<Map> entity = restTemplate.getForEntity("http://api.map.baidu.com/location/ip?ak=UGsiAELjWYyXOkVEERg7vPHXcDfCX7y5&ip="+request.getRemoteAddr(), Map.class);
        Map body = entity.getBody();
        if(CollUtil.isNotEmpty(body)){
            if(Objects.equals( body.get("status"),0)){
                Map content =(Map) body.get("content");
                Object address = content.get("address");
                if(Objects.nonNull(address)){
                    comment.setAddress(address.toString());
                }
            }
        }
        pcCommentService.add(comment);
        return new ResponseResult();
    }
    /**
     * 修改
     * @param comment
     */
    @PostMapping("/update")
    public ResponseResult update(@RequestBody Comment comment){
        pcCommentService.update(comment);
        return new ResponseResult();
    }
    /**
     *  删除
     * @param id
     */
    @GetMapping("/delete")
    public ResponseResult delete(Integer id){
        pcCommentService.delete(id);
        return new ResponseResult();
    }

    /**
     * findArticles
     * @param parentId
     * @param type
     * @return
     */
    @GetMapping("/findArticles")
    public ResponseResult<List<CommentVo>> findArticles(Integer parentId, String type){
        List<CommentVo> articles = pcCommentService.findArticles(parentId, type);
        return ResponseResult.success(articles);
    }

    /**
     * findArticles
     * @param rootId
     * @param type
     * @return
     */
    @GetMapping("/querySumComent")
    public ResponseResult<List<CommentVo>> querySumComent(Integer rootId, String type){
        List<CommentVo> articles = pcCommentService.querySumComent(rootId, type);
        return ResponseResult.success(articles);
    }

}
