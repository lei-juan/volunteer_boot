package com.volunteer.admin.controller;

import com.github.pagehelper.PageInfo;
import com.volunteer.admin.service.UserService;
import com.volunteer.common.entity.Lable;
import com.volunteer.common.entity.User;
import com.volunteer.common.utils.PageResult;
import com.volunteer.common.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/userNew")
public class UserNewController {

    @Autowired
    private UserService userService;

    /**
     * 返回全部lable表记录
     * @return
     */
    @GetMapping("/findAll")
    public List<User> findAll(){
        return userService.findAll();
    }


    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    @GetMapping("/findPage")
    public PageResult<User> findPage(int page, int size){
        PageInfo<User> pages = userService.findPage(page, size);
        return new PageResult<User>(pages.getTotal(),pages.getList());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    @PostMapping("/findList")
    public List<User> findList(@RequestBody Map<String,Object> searchMap){
        return userService.findList(searchMap);
    }
    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */

    @PostMapping("/findPage")
    public PageResult<User> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        PageInfo<User> pages = userService.findPage(searchMap,page, size);
        return new PageResult<User>(pages.getTotal(),pages.getList());
    }
      /**
     * 根据Id查询
     * @param id
     * @return
     */
    @GetMapping("/findById")
    public User findById(Long id){
        return userService.findById(id);
    }


    /**
     * 新增
     */
    @PostMapping("/add")
    public ResponseResult add(@RequestBody User user){
        userService.add(user);
        return new ResponseResult();
    }
    /**
     * 修改
     * @param lable
     */
    @PostMapping("/update")
    public ResponseResult update(@RequestBody User lable){
        userService.update(lable);
        return new ResponseResult();
    }
    /**
     *  删除
     * @param id
     */
    @GetMapping("/delete")
    public ResponseResult delete(Long id){
        userService.delete(id);
        return new ResponseResult();
    }

}
