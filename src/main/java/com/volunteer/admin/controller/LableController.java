package com.volunteer.admin.controller;

import com.volunteer.admin.service.LableService;
import com.volunteer.common.entity.Lable;
import com.volunteer.common.utils.PageResult;
import com.volunteer.common.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping("/lable")
public class LableController {

    @Autowired
    private LableService lableService;

    /**
     * 返回全部lable表记录
     * @return
     */
    @GetMapping("/findAll")
    public List<Lable> findAll(){
        return lableService.findAll();
    }


    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    @GetMapping("/findPage")
    public PageResult<Lable> findPage(int page, int size){
        return lableService.findPage(page, size);
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    @PostMapping("/findList")
    public List<Lable> findList(@RequestBody Map<String,Object> searchMap){
        return lableService.findList(searchMap);
    }
    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */

    @PostMapping("/findPage")
    public PageResult<Lable> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  lableService.findPage(searchMap,page,size);
    }
      /**
     * 根据Id查询
     * @param id
     * @return
     */
    @GetMapping("/findById")
    public Lable findById(Integer id){
        return lableService.findById(id);
    }


    /**
     * 新增
     * @param lable
     */
    @PostMapping("/add")
    public ResponseResult add(@RequestBody Lable lable){
        lable.setCreateTime(new Date());
        lableService.add(lable);
        return new ResponseResult();
    }
    /**
     * 修改
     * @param lable
     */
    @PostMapping("/update")
    public ResponseResult update(@RequestBody Lable lable){
        lable.setCreateTime(new Date());
        lableService.update(lable);
        return new ResponseResult();
    }
    /**
     *  删除
     * @param id
     */
    @GetMapping("/delete")
    public ResponseResult delete(Integer id){
        lableService.delete(id);
        return new ResponseResult();
    }

}
