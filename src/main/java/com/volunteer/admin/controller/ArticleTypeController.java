package com.volunteer.admin.controller;

import com.volunteer.admin.service.ArticleTypeService;
import com.volunteer.common.entity.ArticleType;
import com.volunteer.common.utils.PageResult;
import com.volunteer.common.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping("/articleType")

public class ArticleTypeController {

    @Autowired
    private ArticleTypeService articleTypeService;

    /**
     * 返回全部articleType表记录
     * @return
     */
    @GetMapping("/findAll")
    public List<ArticleType> findAll(){
        return articleTypeService.findAll();
    }


    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    @GetMapping("/findPage")
    public PageResult<ArticleType> findPage(int page, int size){
        return articleTypeService.findPage(page, size);
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    @PostMapping("/findList")
    public List<ArticleType> findList(@RequestBody Map<String,Object> searchMap){
        return articleTypeService.findList(searchMap);
    }
    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */

    @PostMapping("/findPage")
    public PageResult<ArticleType> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  articleTypeService.findPage(searchMap,page,size);
    }
      /**
     * 根据Id查询
     * @param id
     * @return
     */
    @GetMapping("/findById")
    public ArticleType findById(Integer id){
        return articleTypeService.findById(id);
    }


    /**
     * 新增
     * @param articleType
     */
    @PostMapping("/add")
    public ResponseResult add(@RequestBody ArticleType articleType){
        articleType.setCreateTime(new Date());
        articleTypeService.add(articleType);
        return new ResponseResult();
    }
    /**
     * 修改
     * @param articleType
     */
    @PostMapping("/update")
    public ResponseResult update(@RequestBody ArticleType articleType){
        articleType.setCreateTime(new Date());
        articleTypeService.update(articleType);
        return new ResponseResult();
    }
    /**
     *  删除
     * @param id
     */
    @GetMapping("/delete")
    public ResponseResult delete(Integer id){
        articleTypeService.delete(id);
        return new ResponseResult();
    }

}
