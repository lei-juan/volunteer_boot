package com.volunteer.admin.service;

import com.volunteer.common.entity.ArticleType;
import com.volunteer.common.utils.PageResult;

import java.util.*;

/**
 * articleType业务逻辑层
 */
public interface ArticleTypeService {


    public List<ArticleType> findAll();


    public PageResult<ArticleType> findPage(int page, int size);


    public List<ArticleType> findList(Map<String,Object> searchMap);


    public PageResult<ArticleType> findPage(Map<String,Object> searchMap,int page, int size);


    public ArticleType findById(Integer id);

    public void add(ArticleType articleType);


    public void update(ArticleType articleType);


    public void delete(Integer id);

}
