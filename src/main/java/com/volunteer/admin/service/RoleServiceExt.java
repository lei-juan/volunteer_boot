package com.volunteer.admin.service;

import com.volunteer.common.entity.Role;

import java.util.List;

/**
 * @author  volunteer
 * @date  2020/5/13 13:22
 */
public interface RoleServiceExt {


    int updateBatch(List<Role> list);

    int batchInsert(List<Role> list);

    int insertOrUpdate(Role record);

    int insertOrUpdateSelective(Role record);
}
