package com.volunteer.admin.service;
import com.github.pagehelper.PageInfo;
import com.volunteer.common.entity.User;

import java.util.List;
import java.util.Map;

/**
 * user业务逻辑层
 */
public interface UserService extends UserServiceExt{


    public List<User> findAll();


    public PageInfo<User> findPage(int page, int size);


    public List<User> findList(Map<String, Object> searchMap);


    public PageInfo<User> findPage(Map<String, Object> searchMap, int page, int size);


    public User findById(Long id);

    public void add(User user);


    public void update(User user);


    public void delete(Long id);

}
