package com.volunteer.admin.service;

import com.volunteer.common.entity.Article;
import com.volunteer.common.utils.PageResult;

import java.util.*;

/**
 * article业务逻辑层
 */
public interface ArticleService {


    public List<Article> findAll();


    public PageResult<Article> findPage(int page, int size);


    public List<Article> findList(Map<String,Object> searchMap);


    public PageResult<Article> findPage(Map<String,Object> searchMap,int page, int size);


    public Article findById(Integer id);

    public void add(Article article);


    public void update(Article article);


    public void delete(Integer id);

}
