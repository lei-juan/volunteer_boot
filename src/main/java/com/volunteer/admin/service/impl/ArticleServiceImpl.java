package com.volunteer.admin.service.impl;
import com.volunteer.admin.service.ArticleService;
import com.volunteer.common.entity.Article;
import com.volunteer.common.mapper.ArticleMapper;
import com.volunteer.common.utils.PageResult;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleMapper articleMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Article> findAll() {
        return articleMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Article> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Article> articles = (Page<Article>) articleMapper.selectAll();
        return new PageResult<Article>(articles.getTotal(),articles.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Article> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return articleMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Article> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Article> articles = (Page<Article>) articleMapper.selectByExample(example);
        return new PageResult<Article>(articles.getTotal(),articles.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Article findById(Integer id) {
        return articleMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param article
     */
    public void add(Article article) {
        articleMapper.insert(article);
    }

    /**
     * 修改
     * @param article
     */
    public void update(Article article) {
        articleMapper.updateByPrimaryKeySelective(article);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(Integer id) {
        articleMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Article.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 标题
            if(searchMap.get("articleTitle")!=null && !"".equals(searchMap.get("articleTitle"))){
                criteria.andLike("articleTitle","%"+searchMap.get("articleTitle")+"%");
            }
            // 标签
            if(searchMap.get("lableCode")!=null && !"".equals(searchMap.get("lableCode"))){
                criteria.andLike("lableCode","%"+searchMap.get("lableCode")+"%");
            }
            // 封面图
            if(searchMap.get("articleImg")!=null && !"".equals(searchMap.get("articleImg"))){
                criteria.andLike("articleImg","%"+searchMap.get("articleImg")+"%");
            }
            // 内容
            if(searchMap.get("articleContent")!=null && !"".equals(searchMap.get("articleContent"))){
                criteria.andLike("articleContent","%"+searchMap.get("articleContent")+"%");
            }
            // 类别
            if(searchMap.get("typeCode")!=null && !"".equals(searchMap.get("typeCode"))){
                criteria.andLike("typeCode","%"+searchMap.get("typeCode")+"%");
            }
            // 账号
            if(searchMap.get("username")!=null && !"".equals(searchMap.get("username"))){
                criteria.andLike("username","%"+searchMap.get("username")+"%");
            }

            // 主键
            if(searchMap.get("id")!=null ){
                criteria.andEqualTo("id",searchMap.get("id"));
            }
            // 观看数
            if(searchMap.get("views")!=null ){
                criteria.andEqualTo("views",searchMap.get("views"));
            }
            // 点赞数
            if(searchMap.get("likes")!=null ){
                criteria.andEqualTo("likes",searchMap.get("likes"));
            }
            // 评论数
            if(searchMap.get("comments")!=null ){
                criteria.andEqualTo("comments",searchMap.get("comments"));
            }
            // 排序
            if(searchMap.get("sortCode")!=null ){
                criteria.andEqualTo("sortCode",searchMap.get("sortCode"));
            }
            // 是否顶置1是0否
            if(searchMap.get("isTop")!=null ){
                criteria.andEqualTo("isTop",searchMap.get("isTop"));
            }
            // 状态
            if(searchMap.get("status")!=null ){
                criteria.andEqualTo("status",searchMap.get("status"));
            }

        }
        return example;
    }

}
