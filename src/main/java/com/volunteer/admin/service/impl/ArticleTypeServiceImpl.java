package com.volunteer.admin.service.impl;
import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.volunteer.admin.service.ArticleTypeService;
import com.volunteer.common.entity.Article;
import com.volunteer.common.entity.ArticleType;
import com.volunteer.common.mapper.ArticleMapper;
import com.volunteer.common.mapper.ArticleTypeMapper;
import com.volunteer.common.utils.PageResult;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArticleTypeServiceImpl implements ArticleTypeService {

    @Autowired
    private ArticleTypeMapper articleTypeMapper;


    @Autowired
    private ArticleMapper articleMapper;
    /**
     * 返回全部记录
     * @return
     */
    public List<ArticleType> findAll() {
        return articleTypeMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<ArticleType> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<ArticleType> articleTypes = (Page<ArticleType>) articleTypeMapper.selectAll();
        return new PageResult<ArticleType>(articleTypes.getTotal(),articleTypes.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<ArticleType> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return articleTypeMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<ArticleType> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<ArticleType> articleTypes = (Page<ArticleType>) articleTypeMapper.selectByExample(example);
        return new PageResult<ArticleType>(articleTypes.getTotal(),articleTypes.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public ArticleType findById(Integer id) {
        return articleTypeMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param articleType
     */
    public void add(ArticleType articleType) {
        articleTypeMapper.insert(articleType);
    }

    /**
     * 修改
     * @param articleType
     */
    public void update(ArticleType articleType) {
        articleTypeMapper.updateByPrimaryKeySelective(articleType);
    }

    /**
     *  删除
     * @param id
     */
    @Transactional
    public void delete(Integer id) {

        ArticleType articleType = articleTypeMapper.selectByPrimaryKey(id);
        String typeCode = articleType.getTypeCode();
        Example example=new Example(Article.class);
        example.createCriteria().andEqualTo("typeCode",typeCode);
        List<Article> articles = articleMapper.selectByExample(example);
        if(CollUtil.isNotEmpty(articles)){
            throw  new RuntimeException("该分类下还存在活动请先删除活动");
        }

        articleTypeMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(ArticleType.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 类型名称
            if(searchMap.get("typeName")!=null && !"".equals(searchMap.get("typeName"))){
                criteria.andLike("typeName","%"+searchMap.get("typeName")+"%");
            }
            // 类型代码
            if(searchMap.get("typeCode")!=null && !"".equals(searchMap.get("typeCode"))){
                criteria.andLike("typeCode","%"+searchMap.get("typeCode")+"%");
            }

            // id
            if(searchMap.get("id")!=null ){
                criteria.andEqualTo("id",searchMap.get("id"));
            }

        }
        return example;
    }

}
