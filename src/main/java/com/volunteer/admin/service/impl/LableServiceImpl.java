package com.volunteer.admin.service.impl;
import cn.hutool.core.collection.CollUtil;
import com.volunteer.admin.service.LableService;
import com.volunteer.common.entity.Article;
import com.volunteer.common.entity.ArticleType;
import com.volunteer.common.entity.Lable;
import com.volunteer.common.mapper.ArticleMapper;
import com.volunteer.common.mapper.LableMapper;
import com.volunteer.common.utils.PageResult;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class LableServiceImpl implements LableService {

    @Autowired
    private LableMapper lableMapper;
    @Autowired
    private ArticleMapper articleMapper;
    /**
     * 返回全部记录
     * @return
     */
    public List<Lable> findAll() {
        return lableMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Lable> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Lable> lables = (Page<Lable>) lableMapper.selectAll();
        return new PageResult<Lable>(lables.getTotal(),lables.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Lable> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return lableMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Lable> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Lable> lables = (Page<Lable>) lableMapper.selectByExample(example);
        return new PageResult<Lable>(lables.getTotal(),lables.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Lable findById(Integer id) {
        return lableMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param lable
     */
    public void add(Lable lable) {
        lableMapper.insert(lable);
    }

    /**
     * 修改
     * @param lable
     */
    public void update(Lable lable) {
        lableMapper.updateByPrimaryKeySelective(lable);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(Integer id) {

        Lable lable = lableMapper.selectByPrimaryKey(id);
        String lableCode = lable.getLableCode();
        Example example=new Example(Article.class);
        example.createCriteria().andEqualTo("lableCode",lableCode);
        List<Article> articles = articleMapper.selectByExample(example);
        if(CollUtil.isNotEmpty(articles)){
            throw  new RuntimeException("该标签下还存在活动请先删除活动");
        }

        lableMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Lable.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 分类标签
            if(searchMap.get("lableName")!=null && !"".equals(searchMap.get("lableName"))){
                criteria.andLike("lableName","%"+searchMap.get("lableName")+"%");
            }
            // 分类代码
            if(searchMap.get("lableCode")!=null && !"".equals(searchMap.get("lableCode"))){
                criteria.andLike("lableCode","%"+searchMap.get("lableCode")+"%");
            }

            // 主键
            if(searchMap.get("id")!=null ){
                criteria.andEqualTo("id",searchMap.get("id"));
            }
            // 排序（越大越往前）
            if(searchMap.get("sortCode")!=null ){
                criteria.andEqualTo("sortCode",searchMap.get("sortCode"));
            }

        }
        return example;
    }

}
