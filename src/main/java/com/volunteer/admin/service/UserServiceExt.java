package com.volunteer.admin.service;

import com.volunteer.common.entity.Role;
import com.volunteer.common.entity.User;
import com.volunteer.admin.vo.UserVo;

import java.util.List;

/**
 * @author  volunteer
 * @date  2020/5/13 13:12
 */
public interface UserServiceExt {

    UserVo findUserByUserName(String username);

    /**
     * 查询用户是否注册 true 已经注册 false 没有注册
     * @param username
     * @return
     */
    boolean isAccountRegister(String username);


    int updateBatch(List<User> list);

    int batchInsert(List<User> list);

    int insertOrUpdate(User record);

    int insertOrUpdateSelective(User record);

    /**
     * 根用户id查询角色
     * @return List<Role>
     */
    List<Role> findRolesByUserId(Long userId);

    /**
     * 根用户id分配角色
     * @return List<Role>
     */
    void assigningRolesByUserId(Long userId,List<Long> roles);
    /**
     * 最近访问补位
     * @return
     */
    List<User> findFillInUser(int size,List<String> usernames);
    /**
     * 查询user信息
     * @return
     */
    User findUserInfoByUsername(String username);
}
