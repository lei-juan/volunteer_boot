package com.volunteer.admin.service;

import com.volunteer.common.entity.Lable;
import com.volunteer.common.utils.PageResult;

import java.util.*;

/**
 * lable业务逻辑层
 */
public interface LableService {


    public List<Lable> findAll();


    public PageResult<Lable> findPage(int page, int size);


    public List<Lable> findList(Map<String,Object> searchMap);


    public PageResult<Lable> findPage(Map<String,Object> searchMap,int page, int size);


    public Lable findById(Integer id);

    public void add(Lable lable);


    public void update(Lable lable);


    public void delete(Integer id);

}
