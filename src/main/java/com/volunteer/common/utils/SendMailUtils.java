package com.volunteer.common.utils;

import com.volunteer.admin.security.UserDetailsVo;
import com.volunteer.common.constant.JWT;
import com.volunteer.common.entity.MailSend;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author : FallenRunning (汤北寒)
 * @date : 2019/10/22/17:04
 * @desc : 发送邮件的工具类
 */
@Slf4j
@Component
public class SendMailUtils {

    @Autowired
    private JavaMailSender javaMailSender;

    /**
     * 发送一封简单的邮件
     * @param mailSend
     */
    public void sendSimpleMail(MailSend mailSend) {
        try {
            SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
            //邮件发送人
            simpleMailMessage.setFrom(MailSend.SENDER);
            //邮件接收人
            simpleMailMessage.setTo(mailSend.getRecipient());
            //邮件主题
            simpleMailMessage.setSubject(mailSend.getTopic());
            //邮件内容
            simpleMailMessage.setText(mailSend.getContent());
            javaMailSender.send(simpleMailMessage);
        } catch (Exception e) {
            log.error("邮件发送失败 {}", e.getMessage());
        }
    }

    /**
     * @author volunteer
     * @date 2019/10/16 20:23
     */
    @Slf4j
    public static class JwtTokenUtils {

        @Value("${jwt.token.secret}")
        private String secret;

        @Value("${jwt.token.acc_expiration}")
        private int acc_expiration; //过期时长，单位为秒,可以通过配置写入。

        @Value("${jwt.token.ref_expiration}")
        private int ref_expiration; //过期时长，单位为秒,可以通过配置写入。

        public String getUsernameFromToken(String token) {
            String username;
            try {
                username =getClaimsFromToken(token).getSubject();
            } catch (Exception e) {
                username = null;
            }
            return username;
        }

        public Date getCreatedDateFromToken(String token) {
            Date created;
            try {
                final Claims claims = getClaimsFromToken(token);
                created = new Date((Long) claims.get(JWT.CLAIM_KEY_CREATED));
            } catch (Exception e) {
                created = null;
            }
            return created;
        }

        /**
         * 根据token返回spring security Authentication
         * @param token
         * @return
         */
        @SuppressWarnings("unchecked")
        public UsernamePasswordAuthenticationToken getAuthentication(String token) {
            Claims claims =  this.getClaimsFromToken(token);
            String user  = claims.getSubject();
            List<Map> roles = claims.get("roles", List.class);
            List<SimpleGrantedAuthority> auth=new ArrayList<>();
            if(!CollectionUtils.isEmpty(roles)){
                auth= roles.stream().map(entrySet-> new SimpleGrantedAuthority(entrySet.get("authority").toString())).collect(Collectors.toList());
            }
            UserDetailsVo detailsVo=new UserDetailsVo(claims.getSubject(),"",auth);
            detailsVo.setName(String.valueOf(claims.get("name")));
            detailsVo.setPhone(String.valueOf(claims.get("phone")));
            detailsVo.setEmail(String.valueOf(claims.get("email")));
            detailsVo.setId(Long.valueOf(String.valueOf(claims.get("id"))));
            if (user != null) {
                return new UsernamePasswordAuthenticationToken(detailsVo, null, auth!=null?auth:new ArrayList<>());
            }
            return null;
        }

        /**
         * 模拟登录
         * @param accessToken refreshToken
         * @return
         */
        public <T> JwtResult registerSimulatedLogin(String accessToken, String refreshToken, T user) {
            UsernamePasswordAuthenticationToken authentication = this.getAuthentication(accessToken);
            Date date = this.getExpirationDateFromToken(accessToken);
            log.info("******登录成功 ExpirationDate 过期时间:{}",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return JwtResult.success(accessToken, refreshToken,user);
        }
        public Date getExpirationDateFromToken(String token) {
            return getClaimsFromToken(token).getExpiration();
        }

        /**
         * 解析token
         * @param token jwt
         * @return Claims
         */
        public Claims getClaimsFromToken(String token) {
            return  Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        }

        /**
         * 设置
         * @return  acc_token过期时间
         */
        private Date generateAccessTokenExpirationDate() {
            return new Date(System.currentTimeMillis() + acc_expiration * 1000);
        }
        /**
         * 设置
         * @return  ref_token过期时间
         */
        private Date generateRefreshTokenExpirationDate() {
            return new Date(System.currentTimeMillis() + ref_expiration* 1000);
        }
        /**
         * 检查acc_token是否过期 过期会抛出异常
         * @param token jwt
         * @return  true 过期 false 不过期
         */
        public Boolean isTokenExpired(String token) {
            Date expiration = null;
            try {
                expiration = getExpirationDateFromToken(token);
            } catch (Exception e) {
                return true;
            }
            return expiration.before(new Date());
        }

        /**
         * 生成 AccessToken
         * @param authentication
         * @return AccessToken
         */
        public String generateAccessToken(Authentication authentication) {
            Map<String, Object> claims = new HashMap<>();
            claims.put(JWT.CLAIM_KEY_USERNAME, authentication.getName());
            claims.put(JWT.CLAIM_KEY_CREATED, new Date());
            claims.put(JWT.CLAIM_KEY_ID, ((UserDetailsVo)authentication.getPrincipal()).getId());
            claims.put(JWT.CLAIM_KEY_NAME, ((UserDetailsVo)authentication.getPrincipal()).getName());
            claims.put(JWT.CLAIM_KEY_ROLES, authentication.getAuthorities());
            return generateAccessToken(claims);
        }

        /**
         * 生成 AccessToken
         * @param claims
         * @return AccessToken
         */
        public String generateAccessToken(Map<String, Object> claims) {
            return Jwts.builder()
                    .setClaims(claims)
                    .setExpiration(generateAccessTokenExpirationDate())
                    .signWith(SignatureAlgorithm.HS512, secret)
                    .compact();
        }
        /**
         * 生成 RefreshToken
         * @param authentication
         * @return
         */
        public String generateRefreshToken(Authentication authentication) {
            Map<String, Object> claims = new HashMap<>();
            claims.put(JWT.CLAIM_KEY_USERNAME, authentication.getName());
            claims.put(JWT.CLAIM_KEY_CREATED, new Date());
            claims.put(JWT.CLAIM_KEY_ID, ((UserDetailsVo)authentication.getPrincipal()).getId());
            claims.put(JWT.CLAIM_KEY_ROLES, authentication.getAuthorities());
            return generateRefreshToken(claims);
        }

        /**
         * 生成 RefreshToken
         * @param claims
         * @return
         */
        public String generateRefreshToken(Map<String, Object> claims) {
            return Jwts.builder()
                    .setClaims(claims)
                    .setExpiration(generateRefreshTokenExpirationDate())
                    .signWith(SignatureAlgorithm.HS512, secret)
                    .compact();
        }
        /**
         * 更新 AccessToken
         * @param refreshToken
         * @return
         */
        public String updateAccessToken(String refreshToken) {
            Claims newClaims = getClaimsFromToken(refreshToken);
            newClaims.put(JWT.CLAIM_KEY_CREATED, new Date());
            return generateAccessToken(newClaims);
        }
        /**
         * 更新 RefreshToken
         * @param refreshToken
         * @return
         */
        public String updateRefreshToken(String refreshToken) {
            Claims newClaims = getClaimsFromToken(refreshToken);
            newClaims.put(JWT.CLAIM_KEY_CREATED, new Date());
            return generateRefreshToken(newClaims);
        }

        /**
         *
         * @param openid
         * @return
         */
        public String createToken(String openid) {
            Map<String, Object> claims = new HashMap<>();
            claims.put(JWT.CLAIM_KEY_USERNAME, openid);
            claims.put(JWT.CLAIM_KEY_CREATED, new Date());
            claims.put(JWT.CLAIM_KEY_ID, openid);
            return generateAccessToken(claims);
        }
        /**
         * 生成 RefreshToken
         * @param openid
         * @return
         */
        public String createRefreshToken(String openid) {
            Map<String, Object> claims = new HashMap<>();
            claims.put(JWT.CLAIM_KEY_USERNAME, openid);
            claims.put(JWT.CLAIM_KEY_CREATED, new Date());
            claims.put(JWT.CLAIM_KEY_ID, openid);
            return generateRefreshToken(claims);
        }
    }
}
