package com.volunteer.common.utils;

import com.volunteer.common.constant.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * @author : FallenRunning (汤北寒)
 * @date : 2019/10/16/17:21
 */
@Data
@AllArgsConstructor
@ToString
public class ResponseResult<T> {
    /**
     * 状态
     * @link ResponseCode
     */
    private Integer code;
    /**
     * 响应的消息
     */
    private String msg;
    /**
     * 数据
     */
    private T data;
    /**
     * 操作是否成功
     */
    private boolean success;

    private ResponseResult(Integer code, String msg, boolean success){
        this.code = code;
        this.msg = msg;
        this.success = success;
    }
    public ResponseResult(){
        this.code = ResponseCode.SUCCESS;
        this.msg = "ok";
        this.success = true;
    }
    /**
     * 带数据的成功
     * @param data 数据
     * @return ResponseResult
     */
    public static <T> ResponseResult success(T data){
        return new ResponseResult(ResponseCode.SUCCESS,"OK",data,true);//
    }

    /**
     * 成功
     * @return ResponseResult
     */
    public static ResponseResult success(){
        return new ResponseResult(ResponseCode.SUCCESS,"OK",true);
    }

    /**
     * 带消息的失败
     * @return ResponseResult
     */
    public static ResponseResult error(String msg){
        return new ResponseResult(ResponseCode.ERROR,msg,null,false);
    }
    /**
     * 失败
     * @return ResponseResult
     */
    public static ResponseResult error(){
        return new ResponseResult(ResponseCode.ERROR,"NO",false);
    }

    /**
     * 未授权
     * @return ResponseResult
     */
    public static ResponseResult unauthorized(){
        return new ResponseResult(ResponseCode.UNAUTHORIZED,"你没有此操作权限",false);
    }
}
