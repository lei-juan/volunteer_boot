package com.volunteer.common.utils;

import com.volunteer.common.constant.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author : FallenRunning (汤北寒)
 * @date : 2019/10/16/17:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class JwtResult<T> {
    private Integer code;
    private String msg;
    private String access_token;
    private String refresh_token;
    private T data;

    private JwtResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private JwtResult(Integer code, String access_token, String refresh_token) {
        this.code = code;
        this.access_token = access_token;
        this.refresh_token = refresh_token;
    }
    public static <T> JwtResult success(String access_token, String refresh_token, T entity){
        return new JwtResult(ResponseCode.SUCCESS,"OK",access_token,refresh_token,entity);
    }
    public static JwtResult success(String access_token, String refresh_token){
        return new JwtResult(ResponseCode.SUCCESS,access_token,refresh_token);
    }
    public static JwtResult success(String access_token){
        return new JwtResult(ResponseCode.SUCCESS,access_token,null);
    }
    public static JwtResult error(String msg){
        return new JwtResult(ResponseCode.ERROR,msg);
    }
    public static JwtResult invalid(String msg){
        return new JwtResult(ResponseCode.INVALID,msg);
    }
}
