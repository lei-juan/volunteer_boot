package com.volunteer.common.mapper;

import com.volunteer.common.entity.ArticleUser;
import tk.mybatis.mapper.common.Mapper;

public interface ArticleUserMapper extends Mapper<ArticleUser> {

}
