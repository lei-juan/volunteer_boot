package com.volunteer.common.mapper;

import com.volunteer.common.entity.UserLevelExt;
import tk.mybatis.mapper.common.Mapper;

public interface UserLevelExtMapper extends Mapper<UserLevelExt> {

}
