package com.volunteer.common.mapper;

import com.volunteer.common.entity.ArticleType;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author : TBH
 * @date : 2020-11-05-13:57
 */
public interface ArticleTypeMapper extends Mapper<ArticleType> {
}
