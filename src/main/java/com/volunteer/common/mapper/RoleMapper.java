package com.volunteer.common.mapper;

import com.volunteer.common.entity.Role;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

public interface RoleMapper extends Mapper<Role> {

    int updateBatch(List<Role> list);

    int batchInsert(@Param("list") List<Role> list);

    int insertOrUpdate(Role record);

    int insertOrUpdateSelective(Role record);

    List<Role> findRoleByUserId(@Param("userId")Long userId);
}