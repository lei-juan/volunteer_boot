package com.volunteer.common.mapper;

import com.volunteer.common.entity.Comment;
import tk.mybatis.mapper.common.Mapper;

public interface CommentMapper extends Mapper<Comment> {

}
