package com.volunteer.common.mapper;

import com.volunteer.common.entity.UserRole;
import tk.mybatis.mapper.common.Mapper;

public interface UserRoleMapper extends Mapper<UserRole> {

}
