package com.volunteer.common.entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * userRole实体类
 * @author Administrator
 *
 */
@Table(name="tb_user_role")
public class UserRole implements Serializable{

	/**
 	 * id
	*/
	@Id
	private Long id;


	
	/**
 	 * 用户 ID
	*/
	private Long userId;
	/**
 	 * 角色 ID
	*/
	private Long roleId;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}


	
}
