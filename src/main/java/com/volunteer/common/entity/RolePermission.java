package com.volunteer.common.entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * rolePermission实体类
 * @author Administrator
 *
 */
@Table(name="tb_role_permission")
public class RolePermission implements Serializable{

	/**
 	 * id
	*/
	@Id
	private Long id;


	
	/**
 	 * 角色 ID
	*/
	private Long roleId;
	/**
 	 * 权限 ID
	*/
	private Long permissionId;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}


	
}
