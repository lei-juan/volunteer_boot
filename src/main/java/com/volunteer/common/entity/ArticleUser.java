package com.volunteer.common.entity;

import lombok.Data;

import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "tb_article_user")
public class ArticleUser implements Serializable {
    private Integer id;
    private Long userId;
    private Integer articleId;
    private Integer status;
}
