package com.volunteer.common.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * article实体类
 * @author Administrator
 *
 */
@Table(name="tb_article_type")
@Data
public class ArticleType implements Serializable{

	/**
 	 * 主键
	*/
	@Id
	private Integer id;
	/**
 	 * 类型名称
	*/
	private String typeName;
	/**
 	 * 类型代码
	*/
	private String typeCode;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createTime;

}
