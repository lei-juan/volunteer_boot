package com.volunteer.common.entity;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * comment实体类
 * @author Administrator
 *
 */
@Table(name="tb_comment")
public class Comment implements Serializable{

	//id
	@Id
	private Integer id;


	
	//账号
	private String username;
	//评论时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private java.util.Date commentTime;
	//IP地址
	private String ip;
	//位置
	private String address;
	//1帖子0代表评论
	private String type;
	//帖子id或者评论id
	private Integer parentId;
	//根Id
	private Integer rootId;

	private String content;

	public Integer getRootId() {
		return rootId;
	}

	public void setRootId(Integer rootId) {
		this.rootId = rootId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public java.util.Date getCommentTime() {
		return commentTime;
	}
	public void setCommentTime(java.util.Date commentTime) {
		this.commentTime = commentTime;
	}

	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	
}
