package com.volunteer.common.config;

import cn.hutool.json.JSONUtil;
import com.volunteer.common.constant.JWT;
import com.volunteer.admin.security.*;
import com.volunteer.common.utils.ResponseResult;
import com.volunteer.common.utils.SendMailUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.client.RestTemplate;

/**
 * @author volunteer
 * @date 2019/10/16 20:25
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Import(SendMailUtils.JwtTokenUtils.class)
@Slf4j
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${login.url}")
    private String loginUrl;
    @Value("${logout.url}")
    private String logoutUrl;

    @Autowired
    private JWT jwt;

    @Autowired
    private SendMailUtils.JwtTokenUtils jwtTokenUtils;

    @Autowired
    @Qualifier("userDetailsServiceImpl")
    private UserDetailsService userDetailsService;

    @Autowired
    private JWTAuthenticationFilter jwtAuthenticationFilter;

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers()
                .frameOptions().disable();
        //使用jwt的Authentication,来解析过来的请求是否有token

        http.authorizeRequests()
                //这里表示"/any"和"/ignore"不需要权限校验
                .antMatchers(String.join(",", jwt.ignoringUrlAll()).split(",")).permitAll()
                .anyRequest().authenticated()
                .and()
                .cors()
                // 这里表示任何请求都需要校验认证(上面配置的放行)
                .and()
                //配置登录,检测到用户未登录时跳转的url地址,登录放行
                .formLogin()
                //需要跟前端表单的action地址一致
//                .loginPage(loginUrl)
                .loginProcessingUrl(loginUrl)
                .loginPage("/login.html")
                .successHandler(loginSuccessHandler())
                .failureHandler(loginFailureHandler())
                .permitAll()
                //配置取消session管理,又Jwt来获取用户状态,否则即使token无效,也会有session信息,依旧判断用户为登录状态
                .and()
                .logout()
                .logoutUrl(logoutUrl)
                .logoutSuccessHandler((request, response, authentication) -> {
                    String access_token = request.getHeader("X-Token");
                    if(StringUtils.isEmpty(access_token)){
                        access_token = request.getParameter("token");
                    }
                    log.info("***用户{}退出***:",jwtTokenUtils.getUsernameFromToken(access_token));
                    response.setCharacterEncoding("UTF-8");
                    response.setContentType("application/json; charset=utf-8");
                    response.getWriter().print(JSONUtil.toJsonStr(ResponseResult.success()));
                    response.getWriter().flush();
                    response.getWriter().close();
                })
                .and()
                .exceptionHandling()
                .accessDeniedHandler(customizeAccessDeniedHandler())
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                //配置登出,登出放行
                .and()
                .logout()
                .permitAll()
                .and()
                .csrf().disable();

        http.addFilterBefore(jwtAuthenticationFilter,UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(customAuthenticationFilter(),UsernamePasswordAuthenticationFilter.class);
    }
    @Bean
    public CustomAuthenticationFilter customAuthenticationFilter() throws Exception {
        CustomAuthenticationFilter filter=new CustomAuthenticationFilter();
        filter.setAuthenticationManager(authenticationManager());
        filter.setFilterProcessesUrl(loginUrl);
        filter.setAuthenticationSuccessHandler(loginSuccessHandler());
        filter.setAuthenticationFailureHandler(loginFailureHandler());
        return filter;
    }

    @Bean
    public LoginSuccessHandler loginSuccessHandler(){
        return new LoginSuccessHandler();
    }
    @Bean
    public LoginFailureHandler loginFailureHandler(){
        return new LoginFailureHandler();
    }

    @Bean
    public CustomizeAccessDeniedHandler customizeAccessDeniedHandler(){
        return new CustomizeAccessDeniedHandler();
    }

    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                // 设置UserDetailsService
                .userDetailsService(userDetailsService)
                //
                .passwordEncoder(passwordEncoder());
    }
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
