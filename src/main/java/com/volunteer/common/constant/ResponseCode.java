package com.volunteer.common.constant;

/**
 * @author : FallenRunning (汤北寒)
 * @date : 2019/10/16/17:26
 */
public interface ResponseCode {

    /**
     *  请求成功
     */
    Integer SUCCESS = 20000;

    /**
     * 服务端异常
     */
    Integer ERROR = 50000;
    /**
     * 未登录
     */
    Integer UNLOGIN = 50008;
    /**
     * 未授权
     */
    Integer UNAUTHORIZED = 400003;

    /**
     * 已过期
     */
    Integer INVALID = 50014;
}
