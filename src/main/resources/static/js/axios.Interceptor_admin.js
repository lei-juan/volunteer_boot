axios.interceptors.request.use(
    config => {
        // do something before request is sent

        if (localStorage.getItem("admin_token")) {
            // let each request carry token
            // ['X-Token'] is a custom headers key
            // please modify it according to the actual situation
            config.headers['X-Token'] = localStorage.getItem("admin_token")
        }
        return config
    },
    error => {
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)