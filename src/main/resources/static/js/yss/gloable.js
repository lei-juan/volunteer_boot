﻿layui.use(['jquery', 'layer', 'util'], function () {
    var $ = layui.jquery,
        layer = layui.layer,
        util = layui.util;
    util.fixbar();
    //导航控制
    master.start($);
});
var slider = 0;
var pathname = window.location.pathname.replace('Read', 'Article');
var master = {};
master.start = function ($) {
    $('#nav li').hover(function () {
        $(this).addClass('current');
    }, function () {
        var href = $(this).find('a').attr("href");
        if (pathname.indexOf(href) == -1) {
            $(this).removeClass('current');
        }
    });
    selectNav();
    function selectNav() {
        var navobjs = $("#nav a");
        $.each(navobjs, function () {
            var href = $(this).attr("href");
            if (pathname.indexOf(href) != -1) {
                $(this).parent().addClass('current');
            }
        });
    };
    $('.phone-menu').on('click', function () {
        $('#nav').toggle(500);
    });
    $(".blog-user img").hover(function () {
        var tips = layer.tips('点击退出', '.blog-user img', {
            tips: [2, '#009688'],
        });
    }, function () {
        layer.closeAll('tips');
    })
    $(".blog-user .fa-qq").hover(function () {
        var tips = layer.tips('点击登录', '.blog-user .fa-qq', {
            tips: [2, '#009688'],
        });
    }, function () {
        layer.closeAll('tips');
    })
};
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg); //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}
function blogtype() {
    $('#category li').hover(function () {
        $(this).addClass('current');
        var num = $(this).attr('data-index');
        $('.slider').css({ 'top': ((parseInt(num) - 1) * 40) + 'px' });
    }, function () {
        $(this).removeClass('current');
        $('.slider').css({ 'top': slider });
    });
    $(window).scroll(function (event) {
        var winPos = $(window).scrollTop();
        if (winPos > 750)
            $('#categoryandsearch').addClass('fixed');
        else
            $('#categoryandsearch').removeClass('fixed');
    });
};