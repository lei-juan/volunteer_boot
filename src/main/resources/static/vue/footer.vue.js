// 定义一个名为 button-counter 的新组件
Vue.component('footer-bottom', {
    data: function () {
        return {
            count: 0
        }
    },
    template: '<footer class="grid-footer"> ' +
        '  <div class="footer-fixed"> ' +
        '   <div class="copyr0ight"> ' +
        '    <div class="info"> ' +
        '     <div class="contact"> ' +
        '      <a href="javascript:void(0)" class="github" target="_blank"><i class="fa fa-github"></i></a> ' +
        '      <a href="https://wpa.qq.com/msgrd?v=3&uin=930054439&site=qq&menu=yes" class="qq" target="_blank" title="930054439"><i class="fa fa-qq"></i></a> ' +
        '      <a href="https://mail.qq.com/cgi-bin/qm_share?t=qm_mailme&email=gbiysbG0tbWyuMHw8K-i7uw" class="email" target="_blank" title="930054439@qq.com"><i class="fa fa-envelope"></i></a> ' +
        '      <a href="javascript:void(0)" class="weixin"><i class="fa fa-weixin"></i></a> ' +
        '     </div> ' +
        '     <p class="mt05"> ' +
        '     高校志愿者服务系统 ' +
        '     </p> ' +
        '    </div> ' +
        '   </div> ' +
        '  </div> ' +
        ' </footer>'
})