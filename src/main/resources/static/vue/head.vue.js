// 定义一个名为 button-counter 的新组件
Vue.component('head-navigate', {
    data: function () {
        return {
            userInfo:{},
        }
    },
    created(){
        axios.post("/auth/isLogin?token=" + localStorage.getItem('token')).then(response => {
            if(response.data.code!==20000){
                localStorage.removeItem("userInfo")
            }else {
                this.userInfo= JSON.parse(localStorage.getItem("userInfo"))
                this.$forceUpdate();

            }
        }).catch(error=>{
            this.$message({
                message: "服务器异常！",
                type: 'error'
            });

        });
    },
    template: '<header class="gird-header">\n' +
        '        <div class="header-fixed">\n' +
        '            <div class="header-inner">\n' +
        '                <a href="activity.html" class="header-logo" id="logo" >{{userInfo.username!=null?userInfo.username:"feng"}}</a>\n' +
        '                <nav class="nav" id="nav">\n' +
        '                    <ul>\n' +
        '                        <li><a href="user.html" v-if="hasAdmin()">用户管理</a></li>\n' +
        '                        <li><a href="type.html" v-if="hasAdmin()">类型管理</a></li>\n' +
        '                        <li><a href="lable.html" v-if="hasAdmin()">标签管理</a></li>\n' +
        '                        <li><a href="activityVerify.html" v-if="hasAdmin()">活动审核</a></li>\n' +
        '                        <li><a href="write.html" v-if="hasLogin()">发表活动</a></li>\n' +
        '                        <li><a href="activity.html">活动列表</a></li>\n' +
        '                        <li><a href="message.html" v-if="hasHy()">我的活动</a></li>\n' +
        '                        <li><a href="link.html" v-if="hasLogin()">我的信息</a></li>\n' +
        '                    </ul>\n' +
        '                </nav>\n' +
        '                <a href="login.html" class="blog-user">\n' +
        '                    <i v-if="userInfo.avatar==null" class="fa fa-qq"></i>\n' +
        '                </a>\n' +
        '                <a href="login.html" class="blog-user">\n' +
        '                    <i v-if="userInfo.avatar==null" class="fa fa-qq"></i>\n' +
        '                </a>\n' +
        '                <a href="javascript:voide(0)" @click="loginOut()" class="blog-user">\n' +
        '                    <img v-if="userInfo.avatar!=null" class="fa" :src="userInfo.avatar"/>\n' +
        '                </a>\n' +
        '                <a class="phone-menu">\n' +
        '                    <i></i>\n' +
        '                    <i></i>\n' +
        '                    <i></i>\n' +
        '                </a>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </header>',
    updated(){

    },
    mounted(){
    },
    methods:{
        hasLogin(){
            return this.userInfo.id!=null;

        },
        hasAdmin(){
           var result=false;
           if( this.userInfo.roles){
               this.userInfo.roles.forEach(function (item) {
                   if(item.enname=='admin'){
                       result=true;
                   }
               })
           }

            return result;
        },
        hasHy(){
            var result=false;
            if( this.userInfo.roles){
                this.userInfo.roles.forEach(function (item) {
                    if(item.enname=='hy'){
                        result=true;
                    }
                })
            }

            return result;
        },
        loginOut(){
            localStorage.removeItem("token")
            localStorage.removeItem("userInfo")
            window.location.href="/login.html"
        },
    }

})